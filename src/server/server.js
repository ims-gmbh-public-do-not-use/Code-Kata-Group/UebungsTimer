/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 */

'use strict';

const express = require('express');
const os = require('os');

const packageInfos = require('../../package.json');
const restMod = require('./rest');
const logicMod = require('./logic');

//// -------------------------------------- CONFIGURABLE VARIABLES FOR THE SERVER -------------------------------------- ////
// some important configurable information for dashboard, this server and other servers
const ownHostname = os.hostname().toUpperCase();
const productName = packageInfos.name;
const productVersion = packageInfos.version;
const webPort = packageInfos.imsgmbh.webPort;
const appAddress = packageInfos.imsgmbh.webAddress;
const platform = os.platform();

const rest = restMod(express, productName);
const logic = logicMod(rest, false, appAddress);

process.on('uncaughtException', function(err) {
    console.log("uncaughtException:");
    console.log(err);
    console.log(JSON.stringify(err));
    process.exit(1);
});
