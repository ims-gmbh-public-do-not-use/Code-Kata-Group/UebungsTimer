// __dirname is the directory of this file
'use strict';
var fs = require('fs-extra-promise');

var prodName = require(__dirname + '/../package.json').name;

try {
    fs.copySync(__dirname + '/../package.json', process.env.COPYDESTINATION + prodName + '.json');
} catch (err) {
    console.error(err)
}

try {
    fs.copySync(__dirname + '/../dist/' + prodName + '-alpha.zip', process.env.COPYDESTINATION + prodName + '.zip');
} catch (err) {
    console.error(err)
}
