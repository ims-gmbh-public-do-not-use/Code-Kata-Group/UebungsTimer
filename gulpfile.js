/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file Gulpfile
 *
 */

"use strict";

var gulp = require('gulp');
var istanbul = require('gulp-istanbul');
var mocha = require('gulp-mocha');
var fs = require('fs');
var fsExtra = require('fs-extra-promise');
var spawn = require('child_process').spawn;
var stdio = require('stdio');
var csslint = require('gulp-csslint');
var htmllint = require('gulp-htmllint');
var bootlint = require('gulp-bootlint');
var checker = require("license-checker");
var path = require('path');
//var protractor = require("gulp-protractor").protractor;
var runSequence = require('run-sequence');
var eslint = require('gulp-eslint');

var pack = require('./package.json');

function getNodeModules() {
    if (fs.readdirSync("node_modules")) {
        var modules = fs.readdirSync("node_modules");
        var index = modules.indexOf(".bin");
        if (index > -1) {
            modules.splice(index, 1);
        }
        return modules;
    } else {
        return [];
    }
}

gulp.task('createAlphaDist', function (cb) {
    var src = './onlineHelp/' + pack.name + '/html/Manual/';
    var des = "./src/client/Manual/";
    fsExtra.mkdirsSync(des);
    fsExtra.copySync(src, des);
    var cmd = spawn('bin/7z', ['a', '-tzip', 'dist/' + pack.name + '-alpha.zip', '@distfiles.txt'], {stdio: 'inherit'});
    cmd.on('close', function (code) {
        console.log('createAlphaDist exited with code ' + code);
        cb(code);
    });
});

gulp.task('createAllNodeModulesLicenses', function (cb) {
    checker.init({
        "start": __dirname,
        "customPath": "./customFormatExample.json"
    }, function (json, err) {
        if (err) {
            cb(err);
        } else {
            var usedLicenses = [];
            Object.keys(json).forEach(function (item) {
                delete json[item].code;
                delete json[item].path;
                delete json[item].syscall;
                delete json[item].licenseFile;
                delete json[item].repository;
                delete json[item].errno;
                var a= json[item].licenses;
                if (usedLicenses.indexOf(json[item].licenses) < 0) {
                    usedLicenses.push (json[item].licenses);
                }
            });

            fs.writeFileSync('doc/allLicenses.json', JSON.stringify(json, null, 2));
            fs.writeFileSync('doc/usedLicenses.json', JSON.stringify(usedLicenses, null, 2));

            cb();
        }
    });
});

gulp.task('pre-test', function () {
    return gulp.src(['src/server/**/*.js', '!src/server/server.js'])
    // Covering files
        .pipe(istanbul({
            includeUntested:  true
        }))
        // Force `require` to return covered files
        .pipe(istanbul.hookRequire());
});

gulp.task('test', ['pre-test'], function () {
    return gulp.src(['test/server/**/*Test.js'])
        .pipe(mocha())
        // Creating the reports after tests ran
        .pipe(istanbul.writeReports())
        // Enforce a coverage of at least 80%
        .pipe(istanbul.enforceThresholds({thresholds: {global: 83}}));
});

// gulp.task('e2e-dashboard-phantom', function () {
//     return gulp.src(["./test/e2e/dashboard/**/*.js"])
//         .pipe(protractor({
//             configFile: "./test/e2e/dashboard/phantomConfig.js"
//         }))
//         .on('error', function (e) {
//             throw e;
//         });
// });
//
// gulp.task('e2e-dashboard-chrome', function () {
//     return gulp.src(["./test/e2e/dashboard/**/*.js"])
//         .pipe(protractor({
//             configFile: "./test/e2e/dashboard/chromeConfig.js"
//         }))
//         .on('error', function (e) {
//             throw e;
//         });
// });
//
// gulp.task('e2e-dashboard-all', function (cb) {
//     runSequence('e2e-dashboard-phantom', 'e2e-dashboard-chrome', cb);
// });
//
// gulp.task('e2e-comparison-phantom', function () {
//     return gulp.src(["./test/e2e/comparison/**/*.js"])
//         .pipe(protractor({
//             configFile: "./test/e2e/comparison/phantomConfig.js"
//         }))
//         .on('error', function (e) {
//             throw e;
//         });
// });
//
// gulp.task('e2e-comparison-chrome', function () {
//     return gulp.src(["./test/e2e/comparison/**/*.js"])
//         .pipe(protractor({
//             configFile: "./test/e2e/comparison/chromeConfig.js"
//         }))
//         .on('error', function (e) {
//             throw e;
//         });
// });
//
// gulp.task('e2e-comparison-all', function (cb) {
//     runSequence('e2e-comparison-phantom', 'e2e-comparison-chrome', cb);
// });

gulp.task('e2e-rest', function () {
    return gulp.src(["./test/e2e/rest/**/*.js"])
        .pipe(mocha());
});

gulp.task('update-version', function () {
    var cmdOptions = stdio.getopt({
        version: {description: 'Version to write to the package.json file', args: 1}
    });
    if (cmdOptions.version && fs.existsSync('package.json')) {
        var fileContent = fs.readFileSync('package.json');
        var packageObject = JSON.parse(fileContent);
        packageObject.version = cmdOptions.version;
        fs.writeFileSync('package.json', JSON.stringify(packageObject, null, 2));
    }
});

gulp.task('lint-css', function () {
    return gulp.src('src/client/css/**/*.css')
        .pipe(csslint())
        .pipe(csslint.reporter());
});

gulp.task('lint-html', function () {
    return gulp.src('src/client/index.html')
        .pipe(htmllint());
});

gulp.task('lint-bootstrap', function () {
    return gulp.src('src/client/index.html')
        .pipe(bootlint());
});

gulp.task('lint-server', function () {
    // ESLint ignores files with "node_modules" paths.
    // So, it's best to have gulp ignore the directory as well.
    // Also, Be sure to return the stream from the task;
    // Otherwise, the task may end before the stream has finished.
    return gulp.src(['src/server/**/*.js'])
    // eslint() attaches the lint output to the "eslint" property
    // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});

gulp.task('lint-client', function () {
    return gulp.src(['src/client/**/*.js'])
        .pipe(eslint({useEslintrc:true, configFile:'.eslintrc.json'}))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('testAlpha', ['test', 'lint-css', 'lint-html', 'lint-bootstrap', 'lint-server', 'lint-client'], function () {
    console.log('testAlpha fertig.');
});


gulp.task('default', ['testAlpha'], function () {
    console.log('default fertig.');
});

