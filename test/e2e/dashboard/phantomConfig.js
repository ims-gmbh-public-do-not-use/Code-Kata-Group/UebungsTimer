exports.config = {
    framework: 'mocha',
    seleniumServerJar: '../../../../node_modules/protractor/selenium/selenium-server-standalone-2.52.0.jar',
    capabilities:
    {
        'browserName': 'phantomjs',
        'phantomjs.binary.path': "../bin/phantomjs.exe",
    }
};
