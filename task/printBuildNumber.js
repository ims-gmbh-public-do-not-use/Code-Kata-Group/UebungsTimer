/**
 * Created by th on 13.04.2016.
 */

// The printed string is used by TeamCity to set the property build.number
// __dirname is the directory of this file
"use strict";
console.log("##teamcity[buildNumber '" + require(__dirname + '/../package.json').version + "." + process.env.CURRENTBUILDNUMBER + "']");
