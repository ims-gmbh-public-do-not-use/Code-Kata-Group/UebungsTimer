/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file Angular filter for displaying byte numbers as human readable text (2048 Bytes > 2 KB)
*/

'use strict';

function BytesToString() {
    return function (bytes) {
        if (bytes === 0 || bytes === 1) {
            return bytes + ' Byte';
        }

        if (bytes <= 1024) {
            return bytes + ' Bytes';
        }

        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10);
        return (bytes / Math.pow(1024, i)).toFixed(2) + ' ' + sizes[i];
    };
}

angular.module('dashboard').filter('bytesToString', BytesToString);
