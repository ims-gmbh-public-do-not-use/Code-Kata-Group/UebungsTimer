/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file e2e heartbeat test file
 */

"use strict";

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
chai.config.includeStack = true;
global.expect = chai.expect;

var packageInfos = require('../../../package.json');
var util = require('util');


var testUtils = require('./../utils.js')();

var dashboardAddress = 'http://' + packageInfos.imsgmbh.webAddress + ':';
var listenPort = packageInfos.imsgmbh.webPort;
var dashboardName = '/' + packageInfos.name;

describe("dashboard", function () {
    var serviceProcess;

    beforeEach(function (done) {
        this.timeout(5000);
        var startOptions = { port: 0}; // set port to 0 a random port is used
        testUtils.startService(startOptions)
            .then(function (startInfos) {
                serviceProcess = startInfos.process;
                listenPort = startInfos.port;
                done();
            }, function (err) {
                done(err);
            });
    });

    afterEach(function (done) {
        this.timeout(5000);
        testUtils.stopService(serviceProcess)
            .then(function () {
                done();
            }, function (err) {
                done(err);
            });
    });

    it("should be the same tab with index 0", function () {
        this.timeout(5000);

        browser.get(dashboardAddress + listenPort + dashboardName);
        browser.wait(protractor.ExpectedConditions.textToBePresentInElement(element(by.css('.serverStatus')), 'Online'), 2000);

        expect(browser.pixdiff.checkRegion(element(by.id('mainTabs')), 'tabSetTab0')).to.matchScreen();
    });

    it("should be the same tab with index 1 (config)", function () {
        this.timeout(5000);

        browser.get(dashboardAddress + listenPort + dashboardName);
        browser.wait(protractor.ExpectedConditions.textToBePresentInElement(element(by.css('.serverStatus')), 'Online'), 2000);

        element(by.id('mainTabs')).all(by.tagName('li')).get(1).element(by.tagName('a')).click();

        expect(browser.pixdiff.checkRegion(element(by.id('mainTabs')), 'tabSetTab1')).to.matchScreen();
    });
});
