/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2017. All rights reserved.
 */

"use strict";

const sinon = require('sinon');
const chai = require('chai');
chai.config.includeStack = true;
global.expect = chai.expect;
chai.config.includeStack = true;

const mod = require("../../src/server/rest");

describe('', function () {
    let myMod;

    beforeEach(function () {
        myMod = mod(express, productName,...);
    });

    it("should succeed", function () {
        expect(myMod.run()).to.equal(123);
    });
});
