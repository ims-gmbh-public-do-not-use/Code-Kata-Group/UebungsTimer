@REM %~dp0 is the task directory

:delete old
if exist %~dp0..\onlineHelp rd /s /q  %~dp0..\onlineHelp

:create
mkdir %~dp0..\onlineHelp

:clone
set GITCMD=git
if exist \portableGit\bin\git.exe set GITCMD=\portableGit\bin\git
@REM master is the tag to clone:
%GITCMD% clone --single-branch --branch master https://gitlab.com/ims-gmbh-public/OnlineHelp.git %~dp0..\onlineHelp\
rd /S /Q %~dp0..\onlineHelp\.git

:end

