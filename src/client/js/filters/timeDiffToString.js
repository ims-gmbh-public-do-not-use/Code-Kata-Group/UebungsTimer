/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file Angular filter for displaying dates
 */

'use strict';

function TimeDiffToString() {
    return function (timeDiff) {
        if(timeDiff === 0) return '0';
        if (!timeDiff || (timeDiff < 0)) return '-';
        return timeDiff;
    };
}

angular.module('dashboard').filter('timeDiffToString', TimeDiffToString);



angular.module('dashboard').filter('secondsToTimerString', () => {
    const timeFormat = 'mm:ss';
    return function (seconds) {
        if(angular.isDefined(seconds))
            return moment(seconds * 1000).format(timeFormat);
        else {
            return "";
        }
    };
});
