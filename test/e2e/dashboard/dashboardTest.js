/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file e2e heartbeat test file
 */

"use strict";

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
chai.config.includeStack = true;
var expect = chai.expect;

var childProcess = require('child_process');
var packageInfos = require('../../../package.json');

var testUtils = require('./../utils.js')();

var dashboardAddress = 'http://' + packageInfos.imsgmbh.webAddress + ':';
var listenPort = packageInfos.imsgmbh.webPort;
var dashboardName = '/' + packageInfos.name;

describe("dashboard", function () {
    var serviceProcess;

    beforeEach(function (done) {
        this.timeout(5000);
        var startOptions = { port: 0}; // set port to 0 a random port is used
        testUtils.startService(startOptions)
            .then(function (startInfos) {
                serviceProcess = startInfos.process;
                listenPort = startInfos.port;
                done();
            }, function (err) {
                done(err);
            });
    });

    afterEach(function (done) {
        this.timeout(5000);
        testUtils.stopService(serviceProcess)
            .then(function (serviceProcess) {
                done();
            }, function (err) {
                done(err);
            });
    });

    it("should display unknown title in the beginning", function () {
        this.timeout(15000);

        browser.get(dashboardAddress + listenPort + dashboardName);

        expect(browser.getTitle()).to.eventually.equal('Unknown Unknown');
    });

    it("should show unknown status at the beginning", function () {
        this.timeout(5000);

        browser.get(dashboardAddress + listenPort + dashboardName);

        expect(element(by.css('.serverStatus')).getText()).to.eventually.equal('Unknown');
    });

    it("should show online status", function () {
        this.timeout(5000);

        browser.get(dashboardAddress + listenPort + dashboardName);
        browser.wait(protractor.ExpectedConditions.textToBePresentInElement(element(by.css('.serverStatus')), 'Online'), 2000);

        expect(element(by.css('.serverStatus')).getText()).to.eventually.equal('Online');
    });
});
