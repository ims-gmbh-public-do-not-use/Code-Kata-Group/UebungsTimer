@REM %~dp0 is the task directory
@REM current build.number will be given as parameter:
@SETLOCAL
@SET CURRENTBUILDNUMBER=%1%
@del /q "%~dp0..\dist\*-alpha.zip"
@REM @"%~dp0..\node_modules\.bin\gulp" createAlphaDist

call "%~dp0productname.cmd"

@SET SRCH=%~dp0..\onlineHelp\%PRODUCTNAME%\html\manual
@SET DESTH=%~dp0..\src\client\manual\
mkdir "%DESTH%"
xcopy "%SRCH%" "%DESTH%" /S/E/R/Y


"%~dp0..\bin\7z" a -tzip "%~dp0..\dist\%PRODUCTNAME%-alpha.zip" @"%~dp0..\distfiles.txt"
