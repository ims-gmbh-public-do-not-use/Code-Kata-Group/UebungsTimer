/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2017. All rights reserved.
 */

"use strict";

const sinon = require('sinon');
const chai = require('chai');
chai.config.includeStack = true;
global.expect = chai.expect;
chai.config.includeStack = true;

const mod = require("../../src/server/logic");

const rest = {
    get: sinon.stub(),
    post: sinon.stub(),
    listen: sinon.stub()
};

describe('', function () {
    let myMod;

    beforeEach(function () {
        myMod = mod(rest, true);
    });

    it("should succeed getTime", function () {
        let test;
        const res = {
            send: function (x) {
                test = x;
            }
        };
        const expected = {time: 900, teilnehmer: {}};
        myMod.getTime(null, res);
        expect(test).to.deep.equal(expected);
    });
});
