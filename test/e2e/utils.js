/**
 * @copyright IMS Messsysteme GmbH, Copyright (c) 2016. All rights reserved.
 * @file e2e utilities for using in all tests and specs
 */

var kill = require('tree-kill');
var childProcess = require('child_process');
var fsExtra = require('fs-extra-promise');
var q = require('q');
const path = require('path');

module.exports = function () {
    var _obj = {};

    _obj.startService = function (options) {
        var deferred = q.defer();
        var args = [];
        if (options){
            if (options.configuration) {
                var confPath = __dirname + '/temp/utilsConfigFile.json';
                _obj.createConfigurationFile(confPath, options.configuration);
                args.push("--c");
                args.push(confPath);
            }

            if (options.port != undefined) {
                args.push("--port");
                args.push(options.port);
            }

            for (var id in options.args) {
                args.push(options.args[id]);
            }
        }

        // remove debug for child process to allow debugging in webstorm:
        process.execArgv = process.execArgv.filter(function(o){
            var x = "--debug-brk";
            return o.substring(0, x.length) !== x;
        });
        var proc = childProcess.fork(__dirname + '/../../src/server/server.js', args); //, {execArgv: ['--debug-brk=6001']});
        proc.on('message', function (message) {
            if (message.message === 'ServiceStarted') {
                deferred.resolve({process: proc, port: message.port});
            }
        });
        return deferred.promise;
    };

    _obj.createConfigurationFile = function (fullFileName, content) {
        var pathToDir = path.dirname(fullFileName);
        fsExtra.ensureDir(pathToDir);
        if (typeof content === 'object') {
            content = JSON.stringify(content);
        }
        fsExtra.writeFile(fullFileName, content);
    };

    _obj.stopService = function (serviceProcess) {
        var deferred = q.defer();
        kill(serviceProcess.pid, 'SIGKILL', function () {
            deferred.resolve();
        });
        return deferred.promise;
    };

    return _obj;
};
